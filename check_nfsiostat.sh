#!/bin/bash
# Script to make Nagios statistics for Nagios using 'nfsiostat' 
# Written by: Valentino Lauciani (vlauciani@gmail.com)
# Version 1.0
#

### START - Functions ###
declare -A ARRAY_COLUMNS
check_software () {
	if [ -z ${1} ]; then
        	echo ""
	        echo "The software \"${2}\" doesn't exist!!!"
        	echo "Bye"
	        echo ""
        	exit -1
	fi
}

get_nfsiostat_columns_array () {
	VAR=1
	TEXT=""
	for NFSIOSTAT_COLUMN in $( ${NFSIOSTAT} | ${GREP} "Filesystem" | ${AWK} '{print substr($0, index($0, $2))}' ); do 
		ARRAY_COLUMNS[${VAR}]=${NFSIOSTAT_COLUMN}
		VAR=$(( ${VAR} + 1 ))
	done 
}

from_nfsiostat_columns_array_to_variable() {
        TEXT=""
	for i in $( echo ${!ARRAY_COLUMNS[@]} ); do 
		TEXT=${TEXT}$'\n'${i}=${ARRAY_COLUMNS[${i}]}
	done
}

usage() {
cat << EOF
Usage: ${0##*/} [-h] [-b <filesystem> -n <column> -w <warning> -c <critical>]

    -h                    display this help and exit
    -b <Filesystem name>  filesystem to check (grep is used to filter).  Example: "seedstore.waves"
    -n <column to check>  column to check.                               Example: 1=rMB_nor/s or 2=wMB_nor/s, ecc...
    -w <warning>          WARNING threshold in MByte
    -c <critical>         CRITICAL threshold in MByte

    Available column are: ${TEXT}
                
    NOTE: CRITICAL threshold must be greater than WARNING.

    EXAMPLE: ${0##*/} -b wave -n 1 -w 10 -c 100

EOF
}
### END - Functions ###

### START - Check software ###
#NFSIOSTAT=$( which nfsiostat )
NFSIOSTAT=/usr/bin/nfsiostat
check_software "${NFSIOSTAT}" "nfsiostat"
NFSIOSTAT="${NFSIOSTAT} -m " #Adding nfsiostat options

AWK=$( which awk )
check_software "${AWK}" "awk"

GREP=$( which grep )
check_software "${GREP}" "grep"

TAIL=$( which tail )
check_software "${TAIL}" "tail"

BC=$( which bc )
check_software "${BC}" "bc"
### END - Check software ###


# Get available columns
get_nfsiostat_columns_array
from_nfsiostat_columns_array_to_variable

#for i in $( echo ${!ARRAY_COLUMNS[@]} ); do echo ${i} ${ARRAY_COLUMNS[${i}]}; done
#echo -n ${ARRAY_COLUMNS[@]}

# Initialize variables:
FILESYSTEM=
COLUMN=
WARNING=
CRITICAL=

OPTIND=1
while getopts "hb:n:w:c:" OPTION; do
    case "${OPTION}" in
        h)
            usage
            exit 0
            ;;
        b)  FILESYSTEM=${OPTARG}
            ;;
        n)  COLUMN=${OPTARG}
            ;;
        w)  WARNING=${OPTARG}
            ;;
        c)  CRITICAL=${OPTARG}
            ;;
        ?)
	    usage
            exit
            ;;
    esac
done
shift "$((OPTIND-1))" # Shift off the options and optional --

if [[ -z ${FILESYSTEM} ]] || [[ -z ${COLUMN} ]] || [[ -z ${WARNING} ]] || [[ -z ${CRITICAL} ]]; then
     usage
     exit 1
fi

#Read the FIELD value
FIELD_NAME=${ARRAY_COLUMNS[${COLUMN}]}
FIELD_VALUE=$( ${NFSIOSTAT} 8 2 | ${GREP} "${FILESYSTEM}" | ${TAIL} -1 | ${AWK} -v AWK_COLUMN=$(( ${COLUMN} + 1 )) '{print $AWK_COLUMN}' )

if (( $( ${BC} <<< "${FIELD_VALUE} >= ${CRITICAL}" ) == 1 )); then
		echo "CRITICAL: ${FIELD_NAME} = ${FIELD_VALUE} | ${FIELD_NAME}=${FIELD_VALUE};${WARNING};${CRITICAL}"
		exit 2
fi
if (( $( ${BC} <<< "${FIELD_VALUE} >= ${WARNING}" ) == 1 )); then
                echo "WARNING:  ${FIELD_NAME} = ${FIELD_VALUE} | ${FIELD_NAME}=${FIELD_VALUE};${WARNING};${CRITICAL}"
                exit 1
fi
if (( $( ${BC} <<< "${FIELD_VALUE} <= ${WARNING}" ) == 1 )); then
                echo "OK:  ${FIELD_NAME} = ${FIELD_VALUE} | ${FIELD_NAME}=${FIELD_VALUE};${WARNING};${CRITICAL}"
                exit 0
fi
